from file1 import from_file1, from_file11

def from_file2():
    from_file1()

from_file2()

if __name__ == '__main__':
    print(f'i am in the main module file2')
else:
    print(f'I am calling from some other module file2')