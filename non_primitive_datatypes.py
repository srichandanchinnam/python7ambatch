# List
my_list = list()
my_list = []

my_list = [ 10000, 120000.45, 'Ravi K', True ]
print(my_list, type(my_list))


my_list = [ 10000,10000, 120000.45, 'Ravi K', True ]
print(my_list, type(my_list))

print(my_list[0])
print(my_list[-1])

print(my_list[1:4])


# No. of elements  in the list
print(len(my_list))


# for loop
for i in range(len(my_list)):
    print(i)
    print(my_list[i])

print(my_list)

#append method
my_list.append('Sarath S')
my_list.append('Prudhvi')

my_list.append(['Nalini', False, True])
#my_list.append(True, 100000.45)
print(my_list)

# Extend
my_list.extend([True, 100000.45, False])
#my_list.extend('ABC', 'XYZ')
print(my_list)


# insert
print(my_list,"before insert")
my_list.insert(0, "I am adding at 0th index poisition")
my_list.insert(10000, "I am adding at 10000nd index position")
print(my_list,"after insert")
my_list.insert(1,'after 1000nd')



#pop
print(f'my_list before pop : {my_list}')
my_list.pop()
my_list.pop(10)
print(f'my_list after pop : {my_list}')
print(f'my_list after doing pop : {my_list}')
for i in range(len(my_list)):
    my_list.pop()
print(f'my_list after doing pop : {my_list}')

# clear
my_list = ['I am adding at 0th index poisition', 'after 1000nd', 10000, 10000, 120000.45, 'Ravi K', True, 'Sarath S', 'Prudhvi', ['Nalini', False, True], True, 100000.45, False, 'I am adding at 10000nd index position']
my_list.clear()
print(f'my_list after clear method is : {my_list}')
print('my_list after clear method is : {}'.format(my_list))
print('my_list after clear method is :', my_list)


#remove
my_list = [10000, 10000, 120000.45, 'Ravi K', True, 'Sarath S', 'Prudhvi']
my_list.remove('Ravi K')
print(f'my_list after remove method is : {my_list}')


my_list = [10000, 10000, 120000.45, 'Ravi K', True, 'Sarath S', 'Prudhvi','Ravi K', 'Ravi K']
my_list.remove('Ravi K')
my_list.remove('Ravi K')
print(f'my_list after remove method is : {my_list}')


# continue, break
my_list = [10000, 10000, 120000.45, 'Ravi K', True, 'Sarath S', 'Prudhvi']
for i in range(len(my_list)):
    if my_list[i] == 'Ravi K':
        continue
    print(f'=============:{my_list[i]}')
    
for i in range(len(my_list)):
    if my_list[i] == 'Ravi K':
        break
    print(f'***********:{my_list[i]}')


# real time example on break and continue

employee_ids = [ 1234, 4567, 7890, 1234, 9012]
for i in range(len(employee_ids)):
    if employee_ids[i] == 1234:
        continue
    print(f'employee_ids[i] : {employee_ids[i]}')



#Sort
my_list = ['USA', 'UK', 'India', 'Australia', 'France']
my_list.sort()
print(my_list)

my_list.sort(reverse=True)
print(my_list)

# reverse
my_list.reverse()

print('%%%%%%%%%%%%%%%%%%%%')
employee_ids = [ 1234, 4567, 7890, 1234, 9012]

lst = []
for i in range(len(employee_ids)):
    if employee_ids[i] not in lst:
        lst.append(employee_ids[i])


print(lst,"=======")

x = [1,2,3,5,3,2]
y = []
for a in x:
    if not a in y:
       y.append(a)
print(y)


thelist = [1, 2, 3, 4, 4, 5, 5, 6, 1]


duplicate = []
unique = []
for i in thelist:
    if i in unique:
        duplicate.append(i)
    else:
        unique.append(i)
print("Duplicate values: ", duplicate)



# Tuple
my_tuple = tuple()

name = 'Ravi'
print(name, type(name))

name = 'Ravi',
print(name, type(name))

names = ('Ravi','Sarath', 'Prudhvi')
print(names, type(names))


my_list = [1 ,2 ,3 ,4, 'Hi', 10000.45, True, [20,30], [40,50], [60,70], ('Ravi', 'K'), ('Sarath', 'S') ]
my_tuple = ( 1, 2, 3, 4, 'Hi', 10000.45, True, [20,30], [40,50], [60,70], ('Ravi', 'K'), ('Sarath', 'S') )

#Mutable data structure
my_list[0] = 10
print(my_list)


# Immutable data structure
#my_tuple[0] = 10
print(my_tuple)

# iterate
for i in range(len(my_tuple)):
    print(i, "==>", my_tuple[i])

del my_list[0]
print(f'After deleting the 0th index position value is {my_list}')

#del my_tuple[0]
print(f'After deleting the 0th index position value is {my_tuple}')



# Extract the tuple elements
arg1, arg2, arg3, arg4 = tuple(('ABC', 'XYZ', 123, 456))
print(arg1,arg2,arg3,arg4)


# Concatination of Tuples
my_tuple1 = ( 'A' , 'B', 'C')
my_tuple2 = ( 10, 20, 30 )

print(my_tuple1 + my_tuple2)


# Set
my_set = set()

my_set = { 1, 1, 2, 3, 6, 8 , 9}
print(my_set,"removing duplicates")
my_set = { 1, 2, 3, 1.5, 2.5,0, 'Ravi', 'Sarath' , True, False }
print(my_set, type(my_set))

# Access the set elements
#print(my_set[0])

print("---------",bool(1), bool(0))
# Update
#my_set[0] = 'Hello'
print(my_set)

# Delete
#del my_set[0]
print(my_set)

# add
# mutable data structure
my_set.add('Prudhvi')

my_set.add(100)

my_list = [1,2,3]
my_list.append(('Nalini', 'Kumar'))
my_list.extend(('Nalini', 'Kumar'))
print(my_list)
my_set.add(('Nalini', 'Kumar'))
print(my_set)
#my_set.add([120000, 130000])
#print(my_set)


# Update
my_set.update(('Nalini', 'Kumar'))
my_set.update([1200, 1300])
print(my_set)


#set data
my_set = { 10, 12.12, 'Hari', True, ('Ravi','Sarath') }
my_set.update([123,456])
print(my_set)


# remove
my_set.remove('Hari')
#my_set.remove(10000)
my_set.discard(10000)
print(my_set)

#union
my_set1 = {1 ,2 ,3 }
my_set2 = { 'A', 'B', 'C', 1, 2}

my_set = my_set1.union(my_set2)
print(my_set)

# intersection
my_set = my_set1.intersection(my_set2)
print(my_set)

# Diff
my_set = my_set1 - my_set2
print(my_set)

my_set = my_set2 - my_set1
print(my_set)


# Data Database
my_set_from_db = { 1234, 4567, 8908 }

# Data CSV File
my_set_from_file = { 1234, 2123, 5434 }

# superset
A = {1,2,3,4}
#B = {3,4, 10}
B = {3,4}

print(A.issuperset(B))
print(B.issubset(A))


# Dict
my_set = { 'Ravi', 30 }
my_dict = { 'name' : 'Ravi K', 'age' : 30 }
print(my_dict, type(my_dict))