# datastructures
#1) primitive
#2) non-primitive

# Primitive
# int, float, boolean, string

# int datatype
salary = 120000
salary = 130000
print(salary)
print(type(salary))

# float
acc_balance = 120000.12
print(type(acc_balance))

# boolean (True/False)
am_i_learning_python = True
am_i_learning_java = False
print(type(am_i_learning_java), type(am_i_learning_python))

# string datatype
context = 'Hi, i am learning Python course'
print(type(context))
print(context[0])
print(context[-2])

# string slicing
print(context[0:31])
print(context[-3:-1])


# Typecasting
number = '1000'
print(number, type(number))
print(int(number), type(int(number)))


number1 = 2
number2 = 2.5
number3 = number1 + number2 
print(number3, type(number3))

number1 = "2"
number2  = 2.5
number3 = int(number1) + number2
print(number3, type(number3))

salary = 10000
print(str(salary), type(str(salary)))

number = 10
print(bool(number), type(bool(number)))

number = -100
print(bool(number), type(bool(number)))

number = 0
print(bool(number), type(bool(number)))


salary = 12000
print(float(salary), type(float(salary)))