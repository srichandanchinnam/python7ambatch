import logging
logging.basicConfig(filename='D:\\Python\\application.log', filemode='w+', format='%(asctime)s - %(levelname)s - %(message)s',level=logging.INFO)
logger = logging.getLogger(__name__)


print(f'First print statement')

# Logging Levels
logger.info(f'First logger info statement')
logger.debug(f'First logger debug statement')
logger.warning(f'First logger warning statement')
logger.error(f'First logger error statement')
logger.critical(f'First logger critical statement')

number1 = 10
number2 = 0

try:
    number3 = number1/number2
except Exception as e:
    logger.error(e, exc_info=True, stack_info=e)