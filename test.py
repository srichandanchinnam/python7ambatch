print("Hi, Welcome to python course")


# Single line comment
#print("Hi, I am learning Python")

emp_salary = 120000
emp_name = 'Ravi'
emp_acc_balance = 120000.345

# Multiline comment
"""
print(emp_name)
print(emp_salary)
print(emp_acc_balance)
"""

