# Assignment statement
salary = 12000
name = 'Ravi K'
acc_bal = 10000.34

# range 
for i in range(1,11):
    print(i)

# pass
def get_user_name():
    pass

get_user_name()


# Conditional statement
emp1_salary = 10000
emp2_salary = 12000

if emp1_salary == emp2_salary:
    print('emp1 salary and emp2 salary are equal')
else:
    print('salaries are not equal')

# del (delete)
salary = 120000
print(salary)
del salary
#print(salary)

# return statement
def get_user_name():
    return "Ravi K"

name = get_user_name()
print(f'name : {name}')