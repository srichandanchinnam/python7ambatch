Django==3.2.8
mysql-connector-python==8.0.27
sqlparse==0.4.2
pymysql==0.10.1
mysqlclient==2.0.1
djangorestframework==3.12.4
requests==2.27.1
schedule==1.1.0