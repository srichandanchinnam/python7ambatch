from django.urls import path, re_path
from .views import get_name

urlpatterns = [
    path('get_name/', get_name),
]