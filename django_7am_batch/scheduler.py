import schedule
import time
import requests

def job():
    print("I'm working...")
    for url in ['https://api.github.com', 'https://api.github.com/invalid']:
        try:
            response = requests.get(url)
            print(response.status_code,"status_code")
            # If the response was successful, no Exception will be raised
            response.raise_for_status()
        except Exception as err:
            print(f'Other error occurred: {err}')
        else:
            print('Success!')
        print(response)

schedule.every(10).seconds.do(job)
schedule.every(1).minutes.do(job)
# schedule.every().hour.do(job)
# schedule.every().day.at("10:30").do(job)
# schedule.every(5).to(10).minutes.do(job)
# schedule.every().monday.do(job)
# schedule.every().wednesday.at("13:15").do(job)
# schedule.every().minute.at(":17").do(job)

while True:
    schedule.run_pending()
    time.sleep(1)