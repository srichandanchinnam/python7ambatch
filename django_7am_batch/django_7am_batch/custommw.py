

class MyMiddleWare:

    def __init__(self, get_response):
        print(f'MyMiddleware Init Method')
        self.get_response = get_response

    def __call__(self, request):
        print(f'MyMiddleware Call Method')
        print(f'request.session : {request.session}')
        #request.session.clear()
        request.session['name'] = 'Srichandan'
        request.session['email'] = 'srichandan.chinnam@gmail.com'
        response = self.get_response(request)
        response.set_cookie('company_name','Google')
        response.set_cookie('address','Hyd')
        return response