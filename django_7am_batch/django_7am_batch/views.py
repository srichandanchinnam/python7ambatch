from turtle import update
from webbrowser import get
from django.http import JsonResponse, HttpResponse
import json
from django.core import serializers
from django.shortcuts import render
from django_7am_batch.models import Student, Customer, Product
from django.db.models import Count
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView , DeleteView, UpdateView
from django.urls import reverse_lazy
from .forms import StudentForm, LoginForm
from .serializers import StudentSerializer
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
import requests
from requests.exceptions import HTTPError

# function based views
def get_name(request):
    age = get_age()
    data = {'age': age, 'name': 'Hi, i am Ravi'}
    # return JsonResponse(data,safe=False)
    #return HttpResponse(data)
    return JsonResponse(data,safe=False)

def get_name_dynamically(request,  name):
    print(name,"name")
    return JsonResponse(f'Hi, i am {name}',safe=False)


# CreateView, UpdateView, DeleteView

def get_age():
    #return JsonResponse(f'Hi, my age is {age}', safe=False)\
    return f'Hi, my age is 30'


def get_store_details(request):
    return JsonResponse(f'The store id is :12', safe=False)

def month_wise(request,year, month):
    return JsonResponse(f'The year is {year} month is {month}', safe=False)


def create_student(request):
    new_student_details = json.loads(request.body)
    try:
        student_create = Student.objects.create(student_fname=new_student_details['student_fname'],
                student_school_name=new_student_details['student_school_name'],
                student_address=new_student_details['student_address'])
        student_create.save()
        return JsonResponse(f'Student details are created', safe=False)
    except Exception as e:
        return JsonResponse(f'Exception while creating student details',safe=False)

@api_view(['GET', 'POST', 'PUT','DELETE'])
def create_update_delete_get_student(request):
    if request.method == 'GET':
        get_student_details = json.loads(request.body)

        if 'id' in get_student_details:
            # get_student_by_id = Student.objects.get(id=get_student_details['id'])
            # print(get_student_by_id,"get_student_by_id")
            get_student_by_id, bool = Student.objects.get_or_create(id=get_student_details['id'])
            print(get_student_by_id,bool,"get_student_by_id")
            return JsonResponse(f'Student name is : {get_student_by_id}',safe=False)
        else:
            # total_students = Student.objects.count()
            # print(total_students,"total_students")
            get_all_students = Student.objects.all()
            print(get_all_students, type(get_all_students))
            all_students = StudentSerializer(get_all_students, many=True)
            #students = serializers.serialize("json", get_all_students)
            print(all_students.data,"get_all_students")
            return JsonResponse(all_students.data, safe=False)
        return JsonResponse(json.loads(students), safe=False)

    if request.method == 'POST':
        new_student_details = json.loads(request.body)
        # student_create = Student.objects.create(student_fname=new_student_details['student_fname'],
        #         student_lname = new_student_details['student_lname'],
        #         student_school_name=new_student_details['student_school_name'],
        #         student_address=new_student_details['student_address'])
        # student_create.save()
        student_create, bool = Student.objects.get_or_create(student_fname=new_student_details['student_fname'],
                student_lname = new_student_details['student_lname'],
                student_school_name=new_student_details['student_school_name'],
                student_address=new_student_details['student_address'])
        print(student_create, bool,"%%%%%%%%%%%%%%%%%%%%")
        return JsonResponse(f'Student details inserted into DB', safe=False)

    if request.method == 'PUT':
        updated_student_details = json.loads(request.body)
        # update_student, bool = Student.objects.filter(id=updated_student_details['id']).update_or_create(
        #     student_fname=updated_student_details['student_fname'],
        #     student_lname = updated_student_details['student_lname'],
        #     student_school_name=updated_student_details['student_school_name'],
        #     student_address=updated_student_details['student_address']
        # )
        update_student = Student.objects.filter(id=updated_student_details['id']).update(
            student_fname=updated_student_details['student_fname'],
            student_lname = updated_student_details['student_lname'],
            student_school_name=updated_student_details['student_school_name'],
            student_address=updated_student_details['student_address']
        )
        return JsonResponse(f'Student details updated into DB', safe=False)
    if request.method == 'DELETE':
        delete_student_details = json.loads(request.body)
        delete_student = Student.objects.filter(id=delete_student_details['id']).delete()
        return JsonResponse(f'Student details deleted from DB',safe=False)


def usage_of_aggregate(request):
    try:
        # total_purchased_products = Customer.objects.annotate(total_count=Count('purchased_product_name'))
        # products = serializers.serialize("json", total_purchased_products)
        # count_of_products = Customer.objects.aggregate(Count('purchased_product_name'))
        total_purchased_products = Customer.objects.aggregate(Count('purchased_product_name'))
        count_of_products = Customer.objects.filter(id=1).aggregate(Count('purchased_product_name'))
        print(total_purchased_products,"$$$$$$$$$$$$$",count_of_products,"************")
        return JsonResponse(f'Get Customer purchased products from DB',safe=False)
    except Exception as e:
        return JsonResponse(f'Exception is : {str(e)}',safe=False)

def filter_records_matched_with_string(request):
    # records = Student.objects.filter(student_school_name__contains='Na')
    records = Student.objects.filter(student_fname__exact='Ravi')
    print(records,"======")
    return JsonResponse(f'Filtered records from DB',safe=False)
				
def template_view(request):
    # create a dictionary to pass data to the template
    request.session.clear()
    context ={
        # "data":"Template Data by srichandan",
         "data": [1,2,3,4,5,6,7,8,9],
         "username" : request.session['username'],
         "email" : request.session['email']
    }
    return render(request, "test.html", context)


class StudentList(ListView):
    model = Student
    context_object_name = 'students'
    template_name = "django_7am_batch/list_students.html"


class StudentDetailView(DetailView):
    model = Student
    context_object_name = 'student'

class StudentCreateView(CreateView):
    model = Student
    template_name = 'django_7am_batch/student_create.html'
    success_url = reverse_lazy('students_list') #Once student is created, we should be navigating to students list. We should give name of students list  from urls. (From above ListView path('student_list',StudentList.as_view(),name='students') )
    form_class = StudentForm


class StudentUpdateView(UpdateView):
    model = Student
    template_name = 'django_7am_batch/student_create.html'
    success_url = reverse_lazy('students_list') #Once student is created, we should be navigating to students list. We should give name of students list  from urls. (From above ListView path('student_list',StudentList.as_view(),name='students') )
    form_class = StudentForm


class StudentDeleteView(DeleteView):
    model = Student
    context_object_name = 'student'
    template_name = 'django_7am_batch/student_delete.html'
    success_url = reverse_lazy('students_list')




class CreateStudent(APIView):
    def post(self, request):
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": "success", "data": serializer.data},status=status.HTTP_200_OK)
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,id = None):
        request.session.clear()
        if id:
            # print(f"Username from session is : {request.session['username']}")
            # del request.session['username']
            # del request.session['email']
            student = Student.objects.get(id=id)
            serializer = StudentSerializer(student)
            return Response({"status": "success", "data": serializer.data},status=status.HTTP_200_OK)
        students = Student.objects.all()
        print(students,"students")
        serializer = StudentSerializer(students, many=True)
        print(serializer.data,"::::::")
        return Response({"status": "success", "data": serializer.data},status=status.HTTP_200_OK)
			

    def patch(self, request, id = None):
        if id:
            student = Student.objects.get(id=id)
            print(student,"student from db")
            print(request.data,"request.data from chrome")
            serializer = StudentSerializer(student,request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({"status": "success", "data": serializer.data},status=status.HTTP_200_OK)
            else:
                return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request, id = None):
        if id:
            student = Student.objects.filter(id=id).delete()
            return Response({"status": "success", "data": 'Student deleted successfully'},status=status.HTTP_200_OK)


def login(request):
    username = "User is not logged in"
    
    if request.method == "POST":
        #Get the posted form
        my_login_form = LoginForm(request.POST)
        if my_login_form.is_valid():
            username = my_login_form.cleaned_data['username']
            request.session['username'] = username
    else:
        my_login_form = LoginForm()
        
    return render(request, 'loggedin.html', {"username" : username})
    

def check_user_access(request):
    if request.session.has_key('username'):
        username = request.session['username']
        return render(request, 'loggedin.html', {"username" : username})
    else:
        return render(request, 'login.html', {})




def requests_get(request):
    for url in ['https://api.github.com/user', 'https://api.github.com/invalid']:
        try:
            response = requests.get(url, auth=('username', 'password'))
            print(response.status_code,"status_code")
            # If the response was successful, no Exception will be raised
            response.raise_for_status()
        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')
        except Exception as err:
            print(f'Other error occurred: {err}')
        else:
            print('Success!')
        return HttpResponse(response)