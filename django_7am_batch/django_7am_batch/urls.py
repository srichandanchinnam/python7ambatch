"""django_7am_batch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from .views import (
    get_name, get_name_dynamically, get_age,get_store_details, month_wise,
    create_student,create_update_delete_get_student, usage_of_aggregate, filter_records_matched_with_string,
    template_view,
    StudentList,
    StudentDetailView,
    StudentCreateView,
    StudentUpdateView,
    StudentDeleteView,
    CreateStudent,
    check_user_access,
    login,
    requests_get
)
urlpatterns = [
    path('admin/', admin.site.urls),
    path('get_name/', get_name),
    path('get_name_from_args/<str:name>', get_name_dynamically),
    path('get_age/<int:age>', get_age),
    re_path(r'^stores/\d+/',get_store_details),
    re_path(r'^articles/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', month_wise),
    path('create_student',create_student),
    path('create_update_delete_get_student',create_update_delete_get_student),
    path('usage_of_aggregate',usage_of_aggregate),
    path('filter_records_matched_with_string',filter_records_matched_with_string),
    path('load_test_html',template_view),
    path('get_all_students',StudentList.as_view(), name='students_list'),
    path('get_student_information/<str:pk>',StudentDetailView.as_view(), name='student_detail_info'),
    path('create_student_cls_view',StudentCreateView.as_view(), name='create_student'),
    path('update_student_cls_view/<str:pk>',StudentUpdateView.as_view(), name='update_student'),
    path('delete_student_cls_view/<str:pk>',StudentDeleteView.as_view(), name='delete_student'),
    
    
    #drf url
    path('create_student_rest_frame',CreateStudent.as_view(), name='create_student_rf'),
    path('create_student_rest_frame/<int:id>',CreateStudent.as_view()),
    # path('get_student/<int:id>',CreateStudent.as_view()),

    
    path('connection/',check_user_access, name = 'loginform'),
	path('login/', login, name = 'login'),
    path('requests_get',requests_get),
    path('home/', include('home.urls'))
]
