
from django.db import models
import datetime
import time

class Employee(models.Model):

    emp_fname = models.TextField(max_length=30)
    emp_lname = models.TextField(max_length=30)
    emp_age = models.IntegerField()
    emp_email = models.EmailField()

    def __str__(self):
        return self.emp_fname + ' ' + self.emp_lname

#jQjHQgt2UdYdUXr5Lm3T


class Student(models.Model):
    student_fname = models.CharField(max_length=20)
    student_lname = models.CharField(max_length=20)
    student_school_name = models.CharField(max_length=30)
    student_address = models.TextField(default='Hyd', blank=True, null=True)

    def __str__(self):
        return self.student_fname + '  ' + self.student_lname


class ModelField(models.Model):
    bigint_field = models.BigIntegerField()
    binary_field = models.BinaryField()
    char_field = models.CharField(max_length = 200)
    date_field = models.DateField(help_text='YYYY-MM-DD')
    decimal_field = models.DecimalField(  
                    max_digits = 5,
                    decimal_places = 2)
    duration_field = models.DurationField()
    email_field = models.EmailField(max_length = 254)
    upload_field = models.FileField(upload_to ='uploads/')
    float_field = models.FloatField()
    image_field = models.ImageField(upload_to ='uploads/')
    integer_field = models.IntegerField()
    ip_field = models.GenericIPAddressField()
    slug_field = models.SlugField(max_length = 200)
    text_field = models.TextField()
    time_field = models.TimeField()
    url_field = models.URLField(max_length = 200)
    uuid_field = models.UUIDField()
    name = models.CharField(max_length=10,primary_key=True)
    
    def __str__(self):
        return self.name


#Many to one relationship

class Manufacturer(models.Model):
    manufacturer_name = models.CharField(max_length=30)

    def __str__(self):
        return self.manufacturer_name

class CarModel(models.Model):
    car_manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    car_name = models.CharField(max_length=30)

    def __str__(self):
        return self.car_name


#Many to Many Relationship
class Product(models.Model):
    product_name = models.CharField(max_length=30)

    def __str__(self):
        return self.product_name

class Customer(models.Model):
    purchased_product_name = models.ManyToManyField(Product)
    customer_name = models.CharField(max_length=30)

    class Meta:
        db_table = 'Customer'

    def __str__(self):
        return self.customer_name

    


# One - One Relationship
class AadharDetails(models.Model):
    aadhar_number = models.IntegerField()
    user_name = models.CharField(max_length=20)

    def __str__(self):
        return self.user_name

class BankAccount(models.Model):
    aadhar_lnkd_with_bnk_acnt = models.OneToOneField(AadharDetails, on_delete=models.CASCADE)
    bank_name = models.CharField(max_length=20)

    def __str__(self):
        return self.bank_name


class CommonInfo(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField()

    class Meta:
        abstract = True

class Emp(CommonInfo):
    home_group = models.CharField(max_length=5)
    date_field = models.DateField(default=datetime.datetime.now(),blank=True,null=True)

    def __str__(self):
        return self.name

# class SchoolTimings(models.Model):
#     school_name = models.CharField(max_length=30)
#     time_field = models.TimeField(default=time.time.now())