from django.contrib import admin
from .models import (
    Employee, Student,ModelField, Manufacturer, CarModel, Product, Customer, BankAccount,
     AadharDetails, Emp)

admin.site.register(Employee)
admin.site.register(Student)
admin.site.register(ModelField)
admin.site.register(Manufacturer)
admin.site.register(CarModel)
admin.site.register(Product)
admin.site.register(Customer)
admin.site.register(BankAccount)
admin.site.register(AadharDetails)
admin.site.register(Emp)