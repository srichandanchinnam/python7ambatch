import time
import multiprocessing

# def square_of_number(num):
#     a = num * num
#     time.sleep(3)
#     print(f'The value of a is : {a}')

# if __name__ == '__main__':
#     start_time = time.time()
#     print(f'start_time is : {start_time}')
#     square_of_number(2)
#     square_of_number(3)
#     end_time = time.time()
#     print(f'end_time is : {end_time}')
#     print(f'Total execution time is : {end_time - start_time}')


def square_of_number(num):
    a = num * num
    time.sleep(3)
    print(f'The value of a is : {a}')

if __name__ == '__main__':
    start_time = time.time()
    print(f'start_time is : {start_time}')
    # square_of_number(2)
    # square_of_number(3)
    p1 = multiprocessing.Process(target=square_of_number, args=(2,))
    p2 = multiprocessing.Process(target=square_of_number, args=(3,))

    p1.start()
    p2.start()

    p1.join()
    p2.join()
    end_time = time.time()
    print(f'end_time is : {end_time}')
    print(f'Total execution time is : {end_time - start_time}')